﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float Speed = 10f;
	public Vector3 BonusVector;

	void Update ()
	{
		transform.position = transform.position + BonusVector * Speed * Time.deltaTime;		
	}

	void OnCollisionEnter2D(Collision2D collision)
	{

		Destroy(gameObject);

		var col = collision.gameObject.GetComponent<EnemyBall> ();

		if (col)
		{
			col.Size++;
			collision.gameObject.transform.localScale = Vector3.one * 0.05f * col.Size;
		}
	}
}
