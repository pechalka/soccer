﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	private Rigidbody2D _rb;

	public bool Grounded = true;

	public float moveForce = 10f;
	public float maxSpeed = 2f;
	public float jumpForce = 200f;

	void Start ()
	{
		_rb = GetComponent<Rigidbody2D> ();
	}

	void Update ()
	{
		float h = Input.GetAxis("Horizontal");

		if (h * _rb.velocity.x < maxSpeed)
			_rb.AddForce(Vector2.right * h * moveForce);

		if (Input.GetButtonDown ("Jump") && Grounded)
			_rb.AddForce (new Vector2 (0f, jumpForce));
	}
}
