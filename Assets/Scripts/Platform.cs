﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
	void OnCollisionStay2D(Collision2D collision)
	{
		var controller = collision.gameObject.GetComponent<PlayerController> ();

		if (controller)
			controller.Grounded = true;
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		var controller = collision.gameObject.GetComponent<PlayerController> ();

		if (controller)
			controller.Grounded = false;
	}
}
